Pour lancer le projet depuis la branche master, 
lancer la commande: ./run-hsqldb-server.sh

Puis lancer le main de la classe RestApplication

Sur la branche Spring, le code ne compile pas mais j'ai tout de même implémenter la partie spring que je n'ai pas fini par manque de temps.

Je n'ai pas réussi à faire fonctionner docker ni a mettre en oeuvre le système de load balancing nginx.

L'API REST est implémentée mais non exhaustive. Elle recouvre tout de même toutes les opérations possible réparties sur les différentes classes "*Ressource.java".

Le reste du projet à été implémenté.

Pour la partie JPA on retrouve les relations OneToOne, ManyToMany, OneToMany et ManyToOne ainsi que de l'héritage.