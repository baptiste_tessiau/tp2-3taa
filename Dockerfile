FROM maven:3.3.3-jdk-8-onbuild

EXPOSE 8080

CMD java -jar /usr/src/app/target/testjpa-0.0.1-SNAPSHOT.jar