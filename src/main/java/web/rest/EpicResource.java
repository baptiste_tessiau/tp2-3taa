package web.rest;

import domain.Epic;
import io.swagger.annotations.Api;
import repository.EpicRepository;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by btessiau on 27/09/15.
 */

@Path("/epic")
@Api(value="/epic", description = "Epic resource")
public class EpicResource {

    private EpicRepository epicRepository = new EpicRepository();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Epic> getAll() {
        return epicRepository.findAll();
    }


    @GET
    @Path(value = "{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("name") String name) {
        return epicRepository.findOneByName(name)
                .map(e -> Response.ok().entity(e).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }
}
