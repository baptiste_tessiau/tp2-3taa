package web.rest;

import domain.UserStory;
import io.swagger.annotations.Api;
import repository.UserStoryRepository;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by btessiau on 27/09/15.
 */

@Path("/userStory")
@Api(value="/userStory", description = "UserStory resource")
public class UserStoryResource {

    private UserStoryRepository userStoryRepository = new UserStoryRepository();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UserStory> getAll() {
        return userStoryRepository.findAll();
    }

    @GET
    @Path(value = "{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("name") String name) {
        return userStoryRepository.findOneByName(name)
                .map(u -> Response.ok().entity(u).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }

    @POST
    @Path(value = "{name}/{backlogId}/{epicId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response post(@PathParam("name") String name,
                        @PathParam("backlogId") Long backlogId,
                        @PathParam("epicId") Long epicId) {
        if (userStoryRepository.insert(name,backlogId,epicId)) {
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Path(value = "{name}/{backlogId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response post(@PathParam("name") String name,
                        @PathParam("backlogId") Long backlogId) {
        if (userStoryRepository.insert(name,backlogId)) {
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Path(value = "{name}/addTask={taskId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response post2(@PathParam("name") String name,
                        @PathParam("taskId") Long taskId) {
        if (userStoryRepository.insertTask(name, taskId)) {
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @DELETE
    @Path(value = "{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("name") String name) {
        if (userStoryRepository.delete(name)) {
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @DELETE
    @Path(value = "{name}/removeTask={taskId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("name") String name,
                           @PathParam("taskId") Long taskId) {
        if (userStoryRepository.deleteTask(name, taskId)) {
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
