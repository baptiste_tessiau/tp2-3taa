package web.rest;

import domain.Project;
import io.swagger.annotations.Api;
import repository.ProjectRepository;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by btessiau on 27/09/15.
 */

@Path("/project")
@Api(value="/project", description = "Project resource")
public class ProjectResource {

    private ProjectRepository projectRepository = new ProjectRepository();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Project> getAll() {
        return projectRepository.findAll();
    }


    @GET
    @Path(value = "{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("name") String name) {
        return projectRepository.findOneByName(name)
                .map(p -> Response.ok().entity(p).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }

    @POST
    @Path(value = "{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response post(@PathParam("name") String name) {
        if (projectRepository.insert(name)) {
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Path(value = "{name}/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response post(@PathParam("name") String name,
                        @PathParam("userId") Long userId) {
        if (projectRepository.insert(name,userId)) {
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @DELETE
    @Path(value = "{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("name") String name) {
        if (projectRepository.delete(name)) {
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @DELETE
    @Path(value = "{name}/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("name") String name,
                           @PathParam("userId") Long userId) {
        if (projectRepository.delete(name,userId)) {
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
