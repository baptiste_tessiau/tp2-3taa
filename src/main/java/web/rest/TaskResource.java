package web.rest;

import domain.Task;
import io.swagger.annotations.Api;
import repository.TaskRepository;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by btessiau on 27/09/15.
 */

@Path("/task")
@Api(value="/task", description = "Task resource")
public class TaskResource {

    private TaskRepository taskRepository = new TaskRepository();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Task> getAll() {
        return taskRepository.findAll();
    }


    @GET
    @Path(value = "{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("name") String name) {
        return taskRepository.findOneByName(name)
                .map(t -> Response.ok().entity(t).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }

    @POST
    @Path(value = "{name}/{userStoryId}/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response post(@PathParam("name") String name,
                         @PathParam("userStoryId") Long userStoryId,
                         @PathParam("userId") Long userId) {
        if (taskRepository.insert(name,userStoryId,userId)) {
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @DELETE
    @Path(value = "{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("name") String name) {
        if (taskRepository.delete(name)) {
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
