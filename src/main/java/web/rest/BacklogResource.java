package web.rest;

import domain.Backlog;
import io.swagger.annotations.Api;
import repository.BacklogRepository;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by btessiau on 27/09/15.
 */

@Path("/backlog")
@Api(value="/backlog", description = "Backlog resource")
public class BacklogResource {

    private BacklogRepository backlogRepository = new BacklogRepository();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Backlog> getAll() {
        return backlogRepository.findAll();
    }


    @GET
    @Path(value = "{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("name") String name) {
        return backlogRepository.findOneByName(name)
                .map(b -> Response.ok().entity(b).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }
}
