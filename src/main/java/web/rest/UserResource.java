package web.rest;

import domain.User;
import io.swagger.annotations.Api;
import repository.UserRepository;
import service.UserEventService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by btessiau on 27/09/15.
 */

@Path("/user")
@Api(value="/user", description = "User resource")
public class UserResource {

    private UserEventService userEventService = new UserEventService();
    private UserRepository userRepository = new UserRepository();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> getAll() {
        return userRepository.findAll();
    }


    @GET
    @Path(value = "{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("name") String name) {
        return userRepository.findOneByName(name)
                .map(u -> Response.ok().entity(u).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }

    @POST
    @Path(value = "{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response post(@PathParam("name") String name) {
        if (userEventService.insert(name)) {
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Path(value = "{name}/AddProject={project}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postAddProject(@PathParam("name") String name,
                                   @PathParam("project") String project) {
        if (userEventService.addProject(name, project)) {
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Path(value = "{name}/RemoveProject={project}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postRemoveProject(@PathParam("name") String name,
                                      @PathParam("project") String project) {
        if (userEventService.removeProject(name, project)) {
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Path(value = "{name}/AddTask={task}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postAddTask(@PathParam("name") String name,
                                   @PathParam("task") String task) {
        if (userEventService.addTask(name, task)) {
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Path(value = "{name}/RemoveTask={task}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postRemoveTask(@PathParam("name") String name,
                                      @PathParam("task") String task) {
        if (userEventService.removeTask(name, task)) {
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
