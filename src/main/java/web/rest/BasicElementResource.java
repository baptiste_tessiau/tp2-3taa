package web.rest;

import domain.BasicElement;
import io.swagger.annotations.Api;
import repository.BasicElementRepository;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by btessiau on 27/09/15.
 */

@Path("/basicElement")
@Api(value="/basicElement", description = "BasicElement resource")
public class BasicElementResource {

    private BasicElementRepository basicElementRepository = new BasicElementRepository();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<BasicElement> getAll() {
        return basicElementRepository.findAll();
    }


    @GET
    @Path(value = "{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("name") String name) {
        return basicElementRepository.findOneByName(name)
                .map(b -> Response.ok().entity(b).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }
}
