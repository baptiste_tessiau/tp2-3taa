package service;

import domain.Project;
import domain.Task;
import domain.User;
import repository.UserRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Optional;

/**
 * Created by btessiau on 22/10/15.
 */
public class UserEventService {

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("dev");
    EntityManager manager = factory.createEntityManager();

    private UserRepository userRepository = new UserRepository();

    public boolean insert(String name) {
        Optional<User> user = Optional.empty();
        boolean notOtherWithSameName;

        try {
            user = Optional.of(manager.createQuery("SELECT u from User u WHERE u.name = :name", User.class)
                    .setParameter("name", name).getSingleResult());

        } catch (Exception e) {
            // TODO better exception handling
            e.printStackTrace();
        }
        notOtherWithSameName = !(user.isPresent());
        if (notOtherWithSameName) {
            userRepository.insert(name);
        }

        return  notOtherWithSameName;
    }

    public boolean addProject(String name, String projectName) {
        Optional<User> user = Optional.empty();
        boolean notOtherWithSameName = true;

        try {
            user = Optional.of(manager.createQuery("SELECT u from User u WHERE u.name = :name", User.class)
                    .setParameter("name", name).getSingleResult());

        } catch (Exception e) {
            // TODO better exception handling
            e.printStackTrace();
        }

        Optional<Project> project = Optional.empty();

        try {
            project = Optional.of(manager.createQuery("SELECT u from Project u WHERE u.name = :projectName", Project.class)
                    .setParameter("projectName", projectName).getSingleResult());

        } catch (Exception e) {
            // TODO better exception handling
            e.printStackTrace();
        }

        if (user.isPresent() & project.isPresent()) {
            if (!user.get().getProjects().contains(project.get())) {
                notOtherWithSameName = true;
                userRepository.addProject(user.get(),project.get());
            }
        }

        return  notOtherWithSameName;
    }

    public boolean removeProject(String name, String projectName) {
        Optional<User> user = Optional.empty();
        boolean notOtherWithSameName = true;

        try {
            user = Optional.of(manager.createQuery("SELECT u from User u WHERE u.name = :name", User.class)
                    .setParameter("name", name).getSingleResult());

        } catch (Exception e) {
            // TODO better exception handling
            e.printStackTrace();
        }

        Optional<Project> project = Optional.empty();

        try {
            project = Optional.of(manager.createQuery("SELECT u from Project u WHERE u.name = :projectName", Project.class)
                    .setParameter("projectName", projectName).getSingleResult());

        } catch (Exception e) {
            // TODO better exception handling
            e.printStackTrace();
        }

        if (user.isPresent() & project.isPresent()) {
            if (user.get().getProjects().contains(project.get())) {
                notOtherWithSameName = true;
                userRepository.removeProject(user.get(), project.get());
            }
        }

        return  notOtherWithSameName;
    }

    public boolean addTask(String name, String taskName) {
        Optional<User> user = Optional.empty();
        boolean notOtherWithSameName = true;

        try {
            user = Optional.of(manager.createQuery("SELECT u from User u WHERE u.name = :name", User.class)
                    .setParameter("name", name).getSingleResult());

        } catch (Exception e) {
            // TODO better exception handling
            e.printStackTrace();
        }

        Optional<Task> task = Optional.empty();

        try {
            task = Optional.of(manager.createQuery("SELECT u from Task u WHERE u.name = :taskName", Task.class)
                    .setParameter("taskName", taskName).getSingleResult());

        } catch (Exception e) {
            // TODO better exception handling
            e.printStackTrace();
        }

        if (user.isPresent() & task.isPresent()) {
            if (!user.get().getProjects().contains(task.get())) {
                notOtherWithSameName = true;
                userRepository.addTask(user.get(), task.get());
            }
        }

        return  notOtherWithSameName;
    }

    public boolean removeTask(String name, String taskName) {
        Optional<User> user = Optional.empty();
        boolean notOtherWithSameName = true;

        try {
            user = Optional.of(manager.createQuery("SELECT u from User u WHERE u.name = :name", User.class)
                    .setParameter("name", name).getSingleResult());

        } catch (Exception e) {
            // TODO better exception handling
            e.printStackTrace();
        }

        Optional<Task> task = Optional.empty();

        try {
            task = Optional.of(manager.createQuery("SELECT u from Task u WHERE u.name = :taskName", Task.class)
                    .setParameter("taskName", taskName).getSingleResult());

        } catch (Exception e) {
            // TODO better exception handling
            e.printStackTrace();
        }

        if (user.isPresent() & task.isPresent()) {
            if (user.get().getProjects().contains(task.get())) {
                notOtherWithSameName = true;
                userRepository.removeTask(user.get(), task.get());
            }
        }

        return  notOtherWithSameName;
    }

}
