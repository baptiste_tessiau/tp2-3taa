package repository;

import domain.BasicElement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by btessiau on 27/09/15.
 */
public class BasicElementRepository {

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("dev");
    EntityManager manager = factory.createEntityManager();

    public List<BasicElement> findAll() {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<BasicElement> basicElements = new ArrayList<BasicElement>();
        try {
            basicElements = manager.createQuery("SELECT b from BasicElement b", BasicElement.class).getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();

        return basicElements;
    }

    public Optional<BasicElement> findOneByName(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        Optional<BasicElement> basicElements = Optional.empty();
        try {
            basicElements = Optional.of(manager.createQuery("SELECT b from BasicElement b WHERE b.name = :name", BasicElement.class)
                    .setParameter("name", name)
                    .getSingleResult());

        } catch (Exception e) {
            // TODO better exception handling
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();

        return basicElements;
    }

    public void insert(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            BasicElement basicElement = new BasicElement();
            basicElement.setName(name);
            manager.persist(basicElement);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }

    public void delete(BasicElement basicElement) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            manager.remove(basicElement);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }
}
