package repository;

import domain.Epic;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by btessiau on 27/09/15.
 */
public class EpicRepository {

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("dev");
    EntityManager manager = factory.createEntityManager();

    public List<Epic> findAll() {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Epic> epics = new ArrayList<Epic>();
        try {
            epics = manager.createQuery("SELECT e from Epic e", Epic.class).getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();

        return epics;
    }

    public Optional<Epic> findOneByName(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        Optional<Epic> epic = Optional.empty();
        try {
            epic = Optional.of(manager.createQuery("SELECT e from Epic e WHERE e.name = :name", Epic.class)
                    .setParameter("name", name)
                    .getSingleResult());

        } catch (Exception e) {
            // TODO better exception handling
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();

        return epic;
    }

    public void insert(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            Epic epic = new Epic();
            epic.setName(name);
            manager.persist(epic);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }

    public void delete(Epic epic) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            manager.remove(epic);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }
}
