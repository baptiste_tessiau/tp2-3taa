package repository;

import domain.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by btessiau on 27/09/15.
 */
public class ProjectRepository {

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("dev");
    EntityManager manager = factory.createEntityManager();

    public List<Project> findAll() {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Project> projects = new ArrayList<Project>();
        try {
            projects = manager.createQuery("SELECT p from Project p", Project.class).getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();

        return projects;
    }

    public Optional<Project> findOneByName(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        Optional<Project> project = Optional.empty();
        try {
            project = Optional.of(manager.createQuery("SELECT p from Project p WHERE p.name = :name", Project.class)
                    .setParameter("name", name)
                    .getSingleResult());

        } catch (Exception e) {
            // TODO better exception handling
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();

        return project;
    }

    public boolean insert(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            Optional<Project> project = Optional.empty();
            try {
                project = Optional.of(manager.createQuery("SELECT u from Project u WHERE u.name = :name", Project.class)
                        .setParameter("name", name).getSingleResult());

            } catch (Exception e) {
                // TODO better exception handling
                e.printStackTrace();
            }

            if (!project.isPresent()) {
                Project newProject = new Project();
                newProject.setName(name);

                manager.persist(newProject);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        tx.commit();
        manager.close();
        factory.close();
        return true;
    }

    public boolean insert(String name, Long userId) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            Optional<Project> project = Optional.empty();
            try {
                project = Optional.of(manager.createQuery("SELECT u from Project u WHERE u.name = :name", Project.class)
                        .setParameter("name", name).getResultList()
                        .get(0));

            } catch (Exception e) {
                // TODO better exception handling
                e.printStackTrace();
            }

            Optional<User> user = Optional.empty();
            try {
                user = Optional.of(manager.createQuery("SELECT u from User u WHERE u.id = :userId", User.class)
                        .setParameter("userId", userId).getResultList()
                        .get(0));

            } catch (Exception e) {
                // TODO better exception handling
                e.printStackTrace();
            }

            if (project.isPresent() & user.isPresent()) {
                project.get().getUsers().add(user.get());
                user.get().getProjects().add(project.get());
                manager.merge(project.get());
                manager.merge(user.get());
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        tx.commit();
        manager.close();
        factory.close();
        return true;
    }

    public boolean delete(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            Optional<Project> project = Optional.empty();
            try {
                project = Optional.of(manager.createQuery("SELECT u from Project u WHERE u.name = :name", Project.class)
                        .setParameter("name", name).getResultList()
                        .get(0));

            } catch (Exception e) {
                // TODO better exception handling
                e.printStackTrace();
            }

            if (project.isPresent()) {
                Backlog backlog = project.get().getBacklog();
                if (backlog != null) {
                    backlog.setProject(null);
                    manager.merge(backlog);
                }

                for( int i = 0; i < project.get().getUsers().size() ; i ++) {
                    User user = project.get().getUsers().get(i);
                    user.getProjects().remove(project.get());
                    manager.merge(user);
                }

                manager.remove(project.get());
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        tx.commit();
        manager.close();
        factory.close();
        return true;
    }

    public boolean delete(String name, Long userId) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            Optional<Project> project = Optional.empty();
            try {
                project = Optional.of(manager.createQuery("SELECT u from Project u WHERE u.name = :name", Project.class)
                        .setParameter("name", name).getResultList()
                        .get(0));

            } catch (Exception e) {
                // TODO better exception handling
                e.printStackTrace();
            }

            Optional<User> user = Optional.empty();
            try {
                user = Optional.of(manager.createQuery("SELECT u from User u WHERE u.id = :userId", User.class)
                        .setParameter("userId", userId).getResultList()
                        .get(0));

            } catch (Exception e) {
                // TODO better exception handling
                e.printStackTrace();
            }

            if (project.isPresent() & user.isPresent()) {
                project.get().getUsers().remove(user.get());
                user.get().getProjects().remove(project.get());
                manager.merge(project.get());
                manager.merge(user.get());
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        tx.commit();
        manager.close();
        factory.close();
        return true;
    }

    /*public void insert(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            Project project = new Project();
            project.setName(name);
            manager.persist(project);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }*/

    public void delete(Project project) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            manager.remove(project);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }
}
