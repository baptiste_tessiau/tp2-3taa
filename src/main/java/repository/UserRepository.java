package repository;

import domain.Project;
import domain.Task;
import domain.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by btessiau on 27/09/15.
 */
public class UserRepository {

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("dev");
    EntityManager manager = factory.createEntityManager();

    public List<User> findAll() {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<User> users = new ArrayList<User>();
        try {
            users = manager.createQuery("SELECT u from User u", User.class).getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();

        return users;
    }

    public Optional<User> findOneByName(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        Optional<User> user = Optional.empty();
        try {
            user = Optional.of(manager.createQuery("SELECT u from User u WHERE u.name = :name", User.class)
                    .setParameter("name", name)
                    .getSingleResult());

        } catch (Exception e) {
            // TODO better exception handling
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();

        return user;
    }

    public void insert(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
                User user = new User();
                user.setName(name);
                manager.persist(user);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }

    public void delete(User user) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            manager.remove(user);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }

    public void addProject(User user, Project project) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            user.getProjects().add(project);
            project.getUsers().add(user);
            manager.merge(project);
            manager.merge(user);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }

    public void removeProject(User user, Project project) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            user.getProjects().remove(project);
            project.getUsers().remove(user);
            manager.merge(project);
            manager.merge(user);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }

    public void addTask(User user, Task task) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            user.getTasks().add(task);
            task.setUser(user);
            manager.merge(task);
            manager.merge(user);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }

    public void removeTask(User user, Task task) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            user.getTasks().remove(task);
            task.setUser(null);
            manager.merge(task);
            manager.merge(user);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }

}
