package repository;

import domain.Backlog;
import domain.Epic;
import domain.Task;
import domain.UserStory;
import org.hibernate.Query;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by btessiau on 27/09/15.
 */
public class UserStoryRepository {

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("dev");
    EntityManager manager = factory.createEntityManager();

    public List<UserStory> findAll() {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<UserStory> userStories = new ArrayList<UserStory>();
        try {
            userStories = manager.createQuery("SELECT u from UserStory u", UserStory.class).getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();

        return userStories;
    }

    public Optional<UserStory> findOneByName(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        Optional<UserStory> userStory = Optional.empty();
        try {
            userStory = Optional.of(manager.createQuery("SELECT u from UserStory u WHERE u.name = :name", UserStory.class)
                    .setParameter("name", name)
                    .getSingleResult());

        } catch (Exception e) {
            // TODO better exception handling
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();

        return userStory;
    }

    public boolean insert(String name, Long backlogId, Long epicId) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            Optional<UserStory> userStory = Optional.empty();
            try {
                userStory = Optional.of(manager.createQuery("SELECT u from UserStory u WHERE u.name = :name", UserStory.class)
                        .setParameter("name", name).getSingleResult());

            } catch (Exception e) {
                // TODO better exception handling
                e.printStackTrace();
            }

            if (!userStory.isPresent()) {
                UserStory newUserStory = new UserStory();
                newUserStory.setName(name);

                Optional<Backlog> backlog = Optional.empty();
                backlog = Optional.of(manager.createQuery("SELECT u from Backlog u WHERE u.id = :backlogId", Backlog.class)
                        .setParameter("backlogId", backlogId)
                        .getSingleResult());
                if (backlog.isPresent()) {
                    newUserStory.setBacklog(backlog.get());
                }

                Optional<Epic> epic = Optional.empty();
                epic = Optional.of(manager.createQuery("SELECT u from Epic u WHERE u.id = :epicId", Epic.class)
                        .setParameter("epicId", epicId)
                        .getSingleResult());
                if (epic.isPresent()) {
                    newUserStory.setEpic(epic.get());
                }

                manager.persist(newUserStory);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        tx.commit();
        manager.close();
        factory.close();
        return true;
    }

    public boolean insert(String name, Long backlogId) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            Optional<UserStory> userStory = Optional.empty();
            try {
                userStory = Optional.of(manager.createQuery("SELECT u from UserStory u WHERE u.name = :name", UserStory.class)
                        .setParameter("name", name).getSingleResult());

            } catch (Exception e) {
                // TODO better exception handling
                e.printStackTrace();
            }

            if (!userStory.isPresent()) {
                UserStory newUserStory = new UserStory();
                newUserStory.setName(name);
                Optional<Backlog> backlog = Optional.empty();
                backlog = Optional.of(manager.createQuery("SELECT u from Backlog u WHERE u.id = :backlogId", Backlog.class)
                        .setParameter("backlogId", backlogId)
                        .getSingleResult());
                if (backlog.isPresent()) {
                    newUserStory.setBacklog(backlog.get());
                }
                manager.persist(newUserStory);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        tx.commit();
        manager.close();
        factory.close();
        return true;
    }

    public boolean insertTask(String name, Long taskId) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            Optional<UserStory> userStory = Optional.empty();
            try {
                userStory = Optional.of(manager.createQuery("SELECT u from UserStory u WHERE u.name = :name", UserStory.class)
                        .setParameter("name", name).getResultList()
                        .get(0));

            } catch (Exception e) {
                // TODO better exception handling
                e.printStackTrace();
            }
            Optional<Task> task = Optional.empty();
            try {
                task = Optional.of(manager.createQuery("SELECT u from Task u WHERE u.id = :taskId", Task.class)
                        .setParameter("taskId", taskId).getResultList()
                        .get(0));

            } catch (Exception e) {
                // TODO better exception handling
                e.printStackTrace();
            }

            if (userStory.isPresent() & task.isPresent()) {
                task.get().setUserStory(userStory.get());
                userStory.get().getTasks().add(task.get());

                manager.merge(task.get());
                manager.merge(userStory.get());
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        tx.commit();
        manager.close();
        factory.close();
        return true;
    }

    public boolean delete(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            Optional<UserStory> userStory = Optional.empty();
            try {
                userStory = Optional.of(manager.createQuery("SELECT u from UserStory u WHERE u.name = :name", UserStory.class)
                        .setParameter("name", name)
                        .getSingleResult());

            } catch (Exception e) {
                // TODO better exception handling
                e.printStackTrace();
            }
            if (userStory.isPresent()) {
                List<Task> tasks = userStory.get().getTasks();
                for (int i = 0; i < tasks.size(); i++) {
                    manager.remove(tasks.get(i));
                }

                manager.remove(userStory.get());
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        tx.commit();
        manager.close();
        factory.close();
        return true;
    }

    public boolean deleteTask(String name, Long taskId) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            Optional<UserStory> userStory = Optional.empty();
            try {
                userStory = Optional.of(manager.createQuery("SELECT u from UserStory u WHERE u.name = :name", UserStory.class)
                        .setParameter("name", name).getResultList()
                        .get(0));

            } catch (Exception e) {
                // TODO better exception handling
                e.printStackTrace();
            }

            Optional<Task> task = Optional.empty();
            try {
                task = Optional.of(manager.createQuery("SELECT u from Task u WHERE u.id = :taskId", Task.class)
                        .setParameter("taskId", taskId).getResultList()
                        .get(0));

            } catch (Exception e) {
                // TODO better exception handling
                e.printStackTrace();
            }

            if (userStory.isPresent() & task.isPresent()) {
                task.get().setUserStory(null);
                userStory.get().getTasks().remove(userStory.get());
                manager.merge(userStory.get());
                manager.merge(task.get());
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        tx.commit();
        manager.close();
        factory.close();
        return true;
    }

    public void insert(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            UserStory userStory = new UserStory();
            userStory.setName(name);
            manager.persist(userStory);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }

    public void delete(UserStory userStory) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            manager.remove(userStory);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }
}
