package repository;

import domain.Backlog;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by btessiau on 27/09/15.
 */
public class BacklogRepository {

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("dev");
    EntityManager manager = factory.createEntityManager();

    public List<Backlog> findAll() {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Backlog> backlogs = new ArrayList<Backlog>();
        try {
            backlogs = manager.createQuery("SELECT b from Backlog b", Backlog.class).getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();

        return backlogs;
    }

    public Optional<Backlog> findOneByName(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        Optional<Backlog> backlog = Optional.empty();
        try {
            backlog = Optional.of(manager.createQuery("SELECT b from Backlog b WHERE b.name = :name", Backlog.class)
                    .setParameter("name", name)
                    .getSingleResult());

        } catch (Exception e) {
            // TODO better exception handling
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();

        return backlog;
    }

    public void insert(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            Backlog backlog = new Backlog();
            backlog.setName(name);
            manager.persist(backlog);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }

    public void delete(Backlog backlog) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            manager.remove(backlog);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }
}
