package repository;

import domain.Task;
import domain.User;
import domain.UserStory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by btessiau on 27/09/15.
 */
public class TaskRepository {

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("dev");
    EntityManager manager = factory.createEntityManager();

    public List<Task> findAll() {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Task> tasks = new ArrayList<Task>();
        try {
            tasks = manager.createQuery("SELECT t from Task t", Task.class).getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();

        return tasks;
    }

    public Optional<Task> findOneByName(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        Optional<Task> task = Optional.empty();
        try {
            task = Optional.of(manager.createQuery("SELECT t from Task t WHERE t.name = :name", Task.class)
                    .setParameter("name", name)
                    .getSingleResult());

        } catch (Exception e) {
            // TODO better exception handling
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();

        return task;
    }

    public boolean insert(String name, Long userStroryId, Long userId) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            Optional<Task> task = Optional.empty();
            try {
                task = Optional.of(manager.createQuery("SELECT u from Task u WHERE u.name = :name", Task.class)
                        .setParameter("name", name).getSingleResult());

            } catch (Exception e) {
            }

            if (!task.isPresent()) {
                Task newTask = new Task();
                newTask.setName(name);

                Optional<UserStory> userStory = Optional.empty();
                userStory = Optional.of(manager.createQuery("SELECT u from UserStory u WHERE u.id = :userStroryId", UserStory.class)
                        .setParameter("userStroryId", userStroryId)
                        .getSingleResult());
                if (userStory.isPresent()) {
                    newTask.setUserStory(userStory.get());
                }

                Optional<User> user = Optional.empty();
                user = Optional.of(manager.createQuery("SELECT u from User u WHERE u.id = :userId", User.class)
                        .setParameter("userId", userId)
                        .getSingleResult());
                if (user.isPresent()) {
                    newTask.setUser(user.get());
                }

                manager.persist(newTask);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        tx.commit();
        manager.close();
        factory.close();
        return true;
    }

    public boolean delete(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            Optional<Task> task = Optional.empty();
            try {
                task = Optional.of(manager.createQuery("SELECT u from Task u WHERE u.name = :name", Task.class)
                        .setParameter("name", name)
                        .getSingleResult());

            } catch (Exception e) {
                // TODO better exception handling
                e.printStackTrace();
            }
            if (task.isPresent()) {
                UserStory userStory = task.get().getUserStory();
                User user = task.get().getUser();

                if (userStory != null) {
                    userStory.getTasks().remove(task);
                    task.get().setUserStory(null);
                    manager.merge(userStory);
                }

                if (user != null) {
                    user.getTasks().remove(task);
                    task.get().setUser(null);
                    manager.merge(user);
                }
                manager.remove(task.get());
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        tx.commit();
        manager.close();
        factory.close();

        return true;
    }

    public void insert(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            Task task = new Task();
            task.setName(name);
            manager.persist(task);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }

    public void delete(Task task) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            manager.remove(task);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
    }
}
