package domain;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by btessiau on 21/09/15.
 */
@Entity
public class Backlog {

    private Long id;

    private String name;

    private List<UserStory> userStories = new ArrayList<UserStory>();

    private Project project;

    public Backlog() {
        super();
    }

    public Backlog(String name) {
        this.name = name;
    }


    public Backlog(String name, Project project) {
        this.name = name;
        this.project = project;
    }


    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REFRESH}, mappedBy = "backlog")
    @JsonIgnore
    public List<UserStory> getUserStories() {
        return userStories;
    }

    public void setUserStories(List<UserStory> userStories) {
        this.userStories = userStories;
    }


    @OneToOne(cascade = CascadeType.ALL)
    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
