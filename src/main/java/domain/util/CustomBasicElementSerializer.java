package domain.util;

import domain.BasicElement;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

/**
 * Created by leiko on 24/09/15.
 */
public class CustomBasicElementSerializer extends JsonSerializer<BasicElement> {
    @Override
    public void serialize(BasicElement BasicElement, JsonGenerator generator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {
        generator.writeString(BasicElement.getName());
    }
}
