package domain.util;

import domain.UserStory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

/**
 * Created by leiko on 24/09/15.
 */
public class CustomUserStorySerializer extends JsonSerializer<UserStory> {
    @Override
    public void serialize(UserStory userStory, JsonGenerator generator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {
        generator.writeString(userStory.getName());
    }
}
