package domain.util;

import domain.Backlog;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

/**
 * Created by leiko on 24/09/15.
 */
public class CustomBacklogSerializer extends JsonSerializer<Backlog> {
    @Override
    public void serialize(Backlog backlog, JsonGenerator generator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {
        generator.writeString(backlog.getName());
    }
}
