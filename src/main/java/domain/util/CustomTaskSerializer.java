package domain.util;

import domain.Task;
import domain.UserStory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

/**
 * Created by btessiau on 19/10/15.
 */
public class CustomTaskSerializer extends JsonSerializer<Task> {
    @Override
    public void serialize(Task task, JsonGenerator generator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {
        generator.writeString(task.getName());
    }
}
