package domain;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by btessiau on 21/09/15.
 */
@Entity
public class Epic extends UserStory{

    private List<UserStory> userStories = new ArrayList<UserStory>();

    private String name;

    public Epic() {
        super();
    }

    public Epic(String name) {
        this.name = name;
    }

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REFRESH}, mappedBy = "epic")
    @JsonIgnore
    public List<UserStory> getUserStories() {
        return userStories;
    }

    public void setUserStories(List<UserStory> userStories) {
        this.userStories = userStories;
    }

}
