package domain;

import domain.util.CustomUserSerializer;
import domain.util.CustomUserStorySerializer;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * Created by btessiau on 21/09/15.
 */
@Entity
public class Task {

    private Long id;

    private String name;

    private UserStory userStory;

    private User user;

    public Task() {
        super();
    }

    public Task(String name) {
        this.name = name;
    }


    public Task(String name, UserStory userStory) {
        this.name = name;
        this.userStory = userStory;
    }


    public Task(String name, UserStory userStory, User user) {
        this.name = name;
        this.userStory = userStory;
        this.user = user;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne
    @JsonSerialize(using = CustomUserSerializer.class)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    @ManyToOne
    @JsonSerialize(using = CustomUserStorySerializer.class)
    public UserStory getUserStory() {
        return userStory;
    }

    public void setUserStory(UserStory userStory) {
        this.userStory = userStory;
    }

    @Override
    public String toString() {
        return "Task [id=" + id + ", name=" + name + ", userstory="
                + userStory.getName()+ ", user="
                + user.getName() + "]";
    }
}
