package domain;

import domain.util.CustomBacklogSerializer;
import domain.util.CustomUserSerializer;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by btessiau on 25/09/15.
 */
@Entity
public class Project {

    private Long id;

    private String name;

    private List<User> users = new ArrayList<User>();

    private Backlog backlog;

    public Project() {
        super();
    }

    public Project(String name) {
        this.name = name;
    }


    public Project(String name, Backlog backlog) {
        this.name = name;
        this.backlog = backlog;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.REFRESH}, mappedBy = "projects")
    @JsonIgnore
    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @OneToOne(mappedBy = "project")
    @JsonSerialize(using = CustomBacklogSerializer.class)
    public Backlog getBacklog() {
        return backlog;
    }

    public void setBacklog(Backlog backlog) {
        this.backlog = backlog;
    }

    @Override
    public String toString() {
        return "Project [id=" + id + ", name=" + name + ", backlog="
                + backlog.getName() + "]";
    }
}
