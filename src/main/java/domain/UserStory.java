package domain;

import domain.util.CustomBacklogSerializer;
import domain.util.CustomEpicSerializer;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by btessiau on 21/09/15.
 */
@Entity
@Inheritance(strategy= InheritanceType.JOINED)
public class UserStory {

    private Long id;

    private String name;

    private List<Task> tasks = new ArrayList<Task>();

    private Backlog backlog;

    private Epic epic;

    public UserStory() {
        super();
    }

    public UserStory(String name) {
        this.name = name;
    }


    public UserStory(String name, Backlog backlog) {
        this.name = name;
        this.backlog = backlog;
    }


    public UserStory(String name, Backlog backlog, Epic epic) {
        this.name = name;
        this.backlog = backlog;
        this.epic = epic;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "userStory")
    @JsonIgnore
    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    @ManyToOne
    @JsonSerialize(using = CustomEpicSerializer.class)
    public Epic getEpic() {
        return epic;
    }

    public void setEpic(Epic epic) {
        this.epic = epic;
    }

    @ManyToOne
    @JsonSerialize(using = CustomBacklogSerializer.class)
    public Backlog getBacklog() {
        return backlog;
    }

    public void setBacklog(Backlog backlog) {
        this.backlog = backlog;
    }

    @Override
    public String toString() {
        return "UserStory [id=" + id + ", name=" + name + ", backlog="
                + backlog.getName() + ", epic=" + epic.getName() + "]";
    }
}
