package jpa;

import domain.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

public class JpaTestTp2 {

    private EntityManager manager;

    public JpaTestTp2(EntityManager manager) {
        this.manager = manager;
    }

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("dev");
		EntityManager manager = factory.createEntityManager();

		EntityTransaction tx = manager.getTransaction();
		tx.begin();

        JpaTestTp2 test = new JpaTestTp2(manager);

        try {
            test.createContext();

        } catch (Exception e) {
            e.printStackTrace();
        }

		tx.commit();

//        test.listUserStory();

        tx.begin();

        UserStory userStory = manager.createQuery("Select a From UserStory a", UserStory.class).getResultList().get(0);
        manager.remove(userStory);

        tx.commit();

		manager.close();
		factory.close();
	}

    public void createContext() {

        Backlog backlog = new Backlog();
        backlog.setName("backlog");
        Task task = new Task();
        task.setName("task");
        Task task2 = new Task();
        task2.setName("task2");
        Task task3 = new Task();
        task3.setName("task3");
        User user = new User();
        user.setName("user");
        User user2 = new User();
        user2.setName("user2");
        UserStory userStory = new UserStory();
        userStory.setName("userStory");
        Epic epic = new Epic();
        epic.setName("epic");
        Project project = new Project();
        project.setName("project");

        List<UserStory> userStories = new ArrayList<UserStory>();
        userStories.add(userStory);
        backlog.setUserStories(userStories);

        userStory.setEpic(epic);
        userStory.setBacklog(backlog);
        List<Task> tasks = new ArrayList<Task>();
        tasks.add(task);
        userStory.setTasks(tasks);

        user.setTasks(tasks);

        List<Task> tasks2 = new ArrayList<Task>();
        tasks2.add(task2);
        user2.setTasks(tasks2);

        task.setUser(user);
        task.setUserStory(userStory);

        task2.setUser(user2);
        task2.setUserStory(userStory);

        task3.setUser(user2);
        task3.setUserStory(epic);


        List<UserStory> userStoriesForEpic = new ArrayList<UserStory>();
        userStoriesForEpic.add(userStory);
        epic.setUserStories(userStoriesForEpic);
        userStory.setEpic(epic);


        List<Task> tasks3 = new ArrayList<Task>();
        tasks3.add(task3);
        epic.setTasks(tasks3);
        epic.setBacklog(backlog);

        List<Project> projects = new ArrayList<Project>();
        List<User> users = new ArrayList<User>();
        users.add(user);
        users.add(user2);
        projects.add(project);
        project.setUsers(users);

        user.setProjects(projects);
        user2.setProjects(new ArrayList<Project>(projects));


        project.setBacklog(backlog);
        backlog.setProject(project);

        manager.persist(backlog);
        manager.persist(task);
        manager.persist(task2);
        manager.persist(task3);
        manager.persist(user);
        manager.persist(user2);
        manager.persist(userStory);
        manager.persist(epic);
        manager.persist(project);
    }

    private void listUserStory() {
        List<UserStory> resultList = manager.createQuery("Select a From UserStory a", UserStory.class).getResultList();
        System.out.println("num of userStory:" + resultList.size());
        for (UserStory next : resultList) {
            System.out.println("next userStory: " + next);
        }
    }

}
